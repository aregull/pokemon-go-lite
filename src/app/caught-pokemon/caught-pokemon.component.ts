import { Component } from '@angular/core';
import {CaughtPokemonService} from "../services/caught-pokemon.service";
import {Pokemon} from "../shared/models/pokemon.model";
import {Router} from "@angular/router";
import {LoginService} from "../services/login.service";

@Component({
  selector: 'app-caught-pokemon', //registering how you use the component, always stat with "app-"
  templateUrl: './caught-pokemon.component.html',
  styleUrls: ['./caught-pokemon.component.css']
})
export class CaughtPokemonComponent {
  constructor(private readonly caughtPokemonService: CaughtPokemonService,
              private readonly loginService: LoginService,
              private router: Router) {
    if (!this.loginService.alreadyLoggedIn()) {
      this.router.navigate(['login'])
    }
  }

  get caughtPokemon(): Pokemon[] {
    return this.caughtPokemonService.caughtPokemon();
  }

  public capitalizedPokemonName(name: string){
    const capitalized = name.charAt(0).toUpperCase() + name.slice(1)
    return capitalized
  }

  public goToPokemonCatalogue(): void {
    this.router.navigate(['/', 'pokemons'])
  }

  logOut() {
    this.loginService.logOut();
    this.router.navigate(['login'])
  }
}
