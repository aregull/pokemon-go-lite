import {Component} from "@angular/core";
import {NgForm} from "@angular/forms";
import {LoginService} from "../services/login.service";
import {Trainer} from "../shared/models/pokemon-trainer.model";
import {PokemonService} from "../services/pokemon.service";
import {Router} from "@angular/router";


@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['landing-page.component.css']
})
export class LandingPageComponent {

  public name: string = '';
  private trainer: Trainer[] = []
  private newTrainer: Trainer = {name: '', caughtPokemon: []};

  constructor(private readonly loginService: LoginService,
              private readonly pokemonService: PokemonService,
              private readonly router: Router) {
    if (loginService.alreadyLoggedIn()) {
      router.navigate(['pokemons']);
    }
  }

  public onSubmit(): void {
    this.newTrainer.name = this.name;
    localStorage.setItem('poke-trainer', JSON.stringify(this.newTrainer))
    this.loginService.logIn()
    this.router.navigate(['pokemons'])
  }

}
