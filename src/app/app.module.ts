import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {CaughtPokemonComponent} from "./caught-pokemon/caught-pokemon.component";
import {LandingPageComponent} from "./landing-page/landing-page.component";
import {PokemonCatalogueComponent} from "./pokemon-catalogue/pokemon-catalogue.component";
import {PokemonCatalogueItemComponent} from "./pokemon-catalogue-item/pokemon-catalogue-item.component";
import {PokemonsPage} from "./pokemons/pokemons.page";
import {AppRoutingModule} from "./app-routing.module";
import {LoginPage} from "./login/login.page";
import {TrainerPage} from "./trainer/trainer.page";


@NgModule({
  declarations: [
    AppComponent,
    PokemonCatalogueComponent,
    PokemonCatalogueItemComponent,
    CaughtPokemonComponent,
    LandingPageComponent,
    PokemonsPage,
    LoginPage,
    TrainerPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
