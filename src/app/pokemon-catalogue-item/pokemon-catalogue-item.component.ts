import {Component, EventEmitter, Input, Output} from "@angular/core";
import {Pokemon} from "../shared/models/pokemon.model";

@Component({
  selector: 'app-pokemon-catalogue-item',
  templateUrl: './pokemon-catalogue-item.component.html',
  styleUrls: ['./pokemon-catalogue-item.component.css']
})
export class PokemonCatalogueItemComponent {
  @Input() pokemon: Pokemon | undefined; // Will be either a pokemon, or undefined
  @Output() clicked: EventEmitter<Pokemon> = new EventEmitter<Pokemon>();

  public onPokemonClicked(): void {
    this.clicked.emit(this.pokemon);
  }

  public capitalizedPokemonName(name: any) {
    return name.charAt(0).toUpperCase() + name.slice(1)
  }
}
