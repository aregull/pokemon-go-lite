import {Component, OnInit} from "@angular/core";
import {PokemonService} from "../services/pokemon.service";
import {Pokemon} from "../shared/models/pokemon.model";
import {CaughtPokemonService} from "../services/caught-pokemon.service";
import {Router} from "@angular/router";
import {LoginService} from "../services/login.service";

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.component.html',
  styleUrls: ['./pokemon-catalogue.component.css']
})
export class PokemonCatalogueComponent implements OnInit {
  constructor(private readonly pokemonService: PokemonService,
              private readonly caughtPokemonService: CaughtPokemonService,
              private readonly loginService: LoginService,
              private router: Router) {
    if(!this.loginService.alreadyLoggedIn()){
      this.router.navigate(['login'])
    }
  }

  ngOnInit(): void {
    this.pokemonService.fetchAllPokemon();
  }

  public get pokemons(): Pokemon[] {
    return this.pokemonService.pokemon();
  }

  public handlePokemonClicked(pokemon: Pokemon): void {
    let chosenName = prompt("Good catch! Now, give your new Pocket Monster a nickname!")

    if (chosenName === null || chosenName === pokemon.name) {
      alert("Make sure you give it a nickname.")
    } else {
      this.caughtPokemonService.setCaughtPokemon(pokemon, chosenName);
      pokemon.caught = true;
      pokemon.shiny = Math.random() > 0.75;
      pokemon.nickname = chosenName;
    }
  }

  public goToTrainerPage(): void {
    this.router.navigate(['trainer'])
  }

  logOut() {
    this.loginService.logOut()
    this.router.navigate(['login'])
  }
}
