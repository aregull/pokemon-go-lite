import {Injectable} from "@angular/core";
import { Trainer } from "../shared/models/pokemon-trainer.model";
import {Pokemon} from "../shared/models/pokemon.model";

@Injectable({
  providedIn: 'root'
})
export class CaughtPokemonService {
  private _caughtPokemon: Pokemon[] = [];
  private _trainer: Trainer | undefined;

  //This does not work properly, the Match = true part is buggy.
  public setCaughtPokemon(caughtPokemon: Pokemon, name: string): void {
    let match: boolean = false;
    this._caughtPokemon.forEach((alreadyCaughtPokemon) => {
      if (alreadyCaughtPokemon.id === caughtPokemon.id) {
        match = true;
      }
    })

    if (match) {
      return;
    } else if (!match) {
      this._caughtPokemon.push(caughtPokemon);
      this._trainer = JSON.parse(localStorage.getItem('poke-trainer') as string)
      // @ts-ignore
      this._trainer?.caughtPokemon = this._caughtPokemon;
      localStorage.setItem('poke-trainer', JSON.stringify(this._trainer))

    }
  }

  public deleteCaughtPokemon(): void {
    this._caughtPokemon = [];
  }

  public caughtPokemon(): Pokemon[] {
    return this._caughtPokemon
  }
}
