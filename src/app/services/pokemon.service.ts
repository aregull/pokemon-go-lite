import {Injectable} from "@angular/core";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Pokemon} from "../shared/models/pokemon.model";
import {map, tap} from "rxjs/operators";
import {CaughtPokemonService} from "./caught-pokemon.service";

@Injectable({
  providedIn: 'root' //Makes sure this is a singleton object so we share this all over the application
})
export class PokemonService {
  private _pokemons: Pokemon[] = [];
  private _caughtPokemon: Pokemon[] = [];
  private _error: string = '';

  constructor(private readonly http: HttpClient,
              private readonly caughtPokemonService: CaughtPokemonService) {
  }

  private cacheResponse(response: Pokemon[]): void {
    sessionStorage.setItem('poke-cache', JSON.stringify(response))
  }

  private getCachedPokemon(): Pokemon[] | null {
    const cache = sessionStorage.getItem('poke-cache')
    if (cache) {
      return JSON.parse(cache);
    } else {
      return null;
    }
  }

  //Uses observables.
  public fetchAllPokemon(): void {

    this._caughtPokemon = JSON.parse(localStorage.getItem('poke-trainer') as string)
    console.log("CaughtP " + JSON.stringify(this._caughtPokemon));

    const cache: Pokemon[] | null = this.getCachedPokemon();

    if (cache !== null) {
      this._pokemons = [];
      cache.forEach((pokemon: any) =>
        this.getPokemonData(pokemon.name).subscribe((uniqueResponse: any) => {
          this._pokemons.push(uniqueResponse);
        }));
      return;
    }

    this._pokemons = [];
    this.http.get('https://pokeapi.co/api/v2/pokemon/?limit=151')
      .pipe(tap((response: any) => {
        this.cacheResponse(response.results)
      }))
      .subscribe((allPokemon: any) => {
          allPokemon.results.forEach((pokemon: any) =>
            this.getPokemonData(pokemon.name).subscribe((uniqueResponse: any) => {
              this._pokemons.push(uniqueResponse);
            }));
        },
        (error: HttpErrorResponse) => {
          this._error = error.message
        });
  }

  /*
  public fetchAllPokemon(): void {

    this._pokemons = [];
    this.http.get('https://pokeapi.co/api/v2/pokemon/?limit=100')
      .subscribe((allPokemon: any) => {
          allPokemon.results.forEach((pokemon: any) =>
            this.getPokemonData(pokemon.name).subscribe((uniqueResponse: any) => {
              this._pokemons.push(uniqueResponse);
            }));
        },
        (error: HttpErrorResponse) => {
          this._error = error.message
        });
  } */

  public getPokemonData(name: string) {
    return this.http.get<Pokemon[]>(`https://pokeapi.co/api/v2/pokemon/${name}`)
  }

  public pokemon(): Pokemon[] {
    return this._pokemons
  }

  public error(): string {
    return this._error;
  }

}
