export interface Pokemon {
  id: number;
  name: string;
  types: any; //Maybe create an interface for this too, but some pokemon has more than one type.
  sprites: Sprites;
  shinyImage: string;
  height: number;
  weight: number;
  stats: any;
  shiny: boolean;
  caught: boolean;
  nickname: string;
}

export interface Sprites {
  front_default: string;
  front_shiny: string;
}





