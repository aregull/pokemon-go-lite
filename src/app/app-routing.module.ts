import { NgModule } from "@angular/core";
import {RouterModule, Routes, CanActivate} from "@angular/router";


import {PokemonsPage} from "./pokemons/pokemons.page";
import {LoginPage} from "./login/login.page";
import {TrainerPage} from "./trainer/trainer.page";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login'
  },
  {
    path: 'login',
    component: LoginPage
  },
  {
    path: 'pokemons',
    component: PokemonsPage
  },
  {
    path: 'trainer',
    component: TrainerPage
  }
  ];

@NgModule(
  {
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
  }
)
export class AppRoutingModule {}
