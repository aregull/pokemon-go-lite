### Heroku link
So far I've been unsuccessful in deploying the application to Heroku. Will update with link if I get this working.

# Pokemon Go Lite ![pokemon](https://github.com/PokeAPI/sprites/blob/master/sprites/items/poke-doll.png?raw=true)
Welcome to **Pokemon Go Lite**, the application that brings you the thrill of Pokemon Go with the somewhat ease of 
Angular - all from the comfort of your chair! And just as in Pokemon Go, you here have a chance of catching shiny pokemons. Just keep on catching,
the shiny ones will reveal themselves upon being caught.

## Now, how do I play?
Easy! Clone this project by copying this command to your terminal of choice!

`git clone git@gitlab.com:aregull/pokemon-go-lite`

Once this is done, write these two commands in the same terminal

`npm install`

`ng serve`

Now, if your browser doesn't open on its own, go to [localhost:4200](https://localhost:4200) and let the fun begin!

## ... and what are the rules?
1. Log in by entering your desired username on the landing page.
2. To catch a Pokemon, you must give it a nickname. Enter the desired nickname in the
prompt that opens when you click a Pokemon. The name _cannot_ be empty or match the Pokemons
original name.
3. Refresh the site and do it all again! How fun!

--- 

# Reflections ![pokeball](https://github.com/PokeAPI/sprites/blob/master/sprites/items/premier-ball.png?raw=true)
As a part of the learning process, I've included some thoughts about my work.

## What has been my focus on this project?
I've focused on making this project make better use of **semantic HTML-tags** more than before, e.g. I've steered away 
from using div-tags when I can use section instead. I have also put more effort in making more **readable variable
names**, to make it easier to understand the bigger picture and the data flow in this application. People make mistakes,
and so do I, so when I find variables or functions with a name I could improve, I **refactor the code**. I have also tried to make a
**structured, fun and readable README**-file. 

## What could have been done better?
I think there's room for improvements when showing a single Pokemon in both the _Pokemon Catalogue_ and on the _Trainer Page_.
It could be useful to know the HP or Attack Stats of every Pokemon. The PokeAPI has a lot of information
that could be useful and fun to incorporate. 

## Ideas for further development
A fun idea would be to randomize the order the Pokemon are showed, limit it to 50, and remove the evolved states of
every Pokemon. On every refresh, a new batch of Pokemon are displayed, and if you catch two of the same Pokemon that are able
to evolve, you will get the evolved variant. This would require better handling of the state, as the application now
wipes everything on refresh. 

Another idea is to make an image carousel on each Pokemon, making use of the images from the PokeAPI. As far as I've seen, most
Pokemon has a front and back image. This could let the user see how the Pokemon looks from behind as well.

---
# Resources
PokeAPI: https://pokeapi.co/

Background images: https://imgur.com/a/6c481

## Todo
- Fix bug where pokemon gets fetched every time you go to Pokemon Catalogue
- Make pokemon catalogue item and caught pokemon display look the same.
- Fix so that only pokemon picture is scaled up, not pokeball when hovering over pokemon
- Make the app remember which pokemon has been caught
